# Changelog

## 0.0.2 (07 Nov 2020)

* Fixed: License badge in readme displaying wrong license
* Fixed: Missing second paragraph in readme that we somehow lost
* Updated: Git URLs to reflect changes in the repositories

## 0.0.1 (27 Oct 2020)

* Initial Release
